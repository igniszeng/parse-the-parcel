﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParseTheParcel
{
    public class PackageSolutionManager
    {
        #region variables of Limits

        // These variables will only be initialized once from the resources.resx file
        // when this class is accessed for the first time.

        private static bool _isMaxSmallLengthInitialized = false;
        private static int _maxSmallLength;
        /// <summary>
        /// Length limit of small size. Unit: mm.
        /// </summary>
        public static int MaxSmallLength
        {
            get
            {
                if (_isMaxSmallLengthInitialized == false)
                {
                    _maxSmallLength = int.Parse(Properties.Resources.MaxSmallLength);
                    _isMaxSmallLengthInitialized = true;
                }
                return _maxSmallLength;
            }
        }

        private static bool _isMaxSmallBreadthInitialized = false;
        private static int _maxSmallBreadth;
        /// <summary>
        /// Breadth limit of small size. Unit: mm.
        /// </summary>
        public static int MaxSmallBreadth
        {
            get
            {
                if (_isMaxSmallBreadthInitialized == false)
                {
                    _maxSmallBreadth = int.Parse(Properties.Resources.MaxSmallBreadth);
                    _isMaxSmallBreadthInitialized = true;
                }
                return _maxSmallBreadth;
            }
        }

        private static bool _isMaxSmallHeightInitialized = false;
        private static int _maxSmallHeight;
        /// <summary>
        /// Height limit of small size. Unit: mm.
        /// </summary>
        public static int MaxSmallHeight
        {
            get
            {
                if (_isMaxSmallHeightInitialized == false)
                {
                    _maxSmallHeight = int.Parse(Properties.Resources.MaxSmallHeight);
                    _isMaxSmallHeightInitialized = true;
                }
                return _maxSmallHeight;
            }
        }



        private static bool _isMaxMediumLengthInitialized = false;
        private static int _maxMediumLength;
        /// <summary>
        /// Length limit of medium size. Unit: mm.
        /// </summary>
        public static int MaxMediumLength
        {
            get
            {
                if (_isMaxMediumLengthInitialized == false)
                {
                    _maxMediumLength = int.Parse(Properties.Resources.MaxMediumLength);
                    _isMaxMediumLengthInitialized = true;
                }
                return _maxMediumLength;
            }
        }

        private static bool _isMaxMediumBreadthInitialized = false;
        private static int _maxMediumBreadth;
        /// <summary>
        /// Breadth limit of medium size. Unit: mm.
        /// </summary>
        public static int MaxMediumBreadth
        {
            get
            {
                if (_isMaxMediumBreadthInitialized == false)
                {
                    _maxMediumBreadth = int.Parse(Properties.Resources.MaxMediumBreadth);
                    _isMaxMediumBreadthInitialized = true;
                }
                return _maxMediumBreadth;
            }
        }

        private static bool _isMaxMediumHeightInitialized = false;
        private static int _maxMediumHeight;
        /// <summary>
        /// Height limit of medium size. Unit: mm.
        /// </summary>
        public static int MaxMediumHeight
        {
            get
            {
                if (_isMaxMediumHeightInitialized == false)
                {
                    _maxMediumHeight = int.Parse(Properties.Resources.MaxMediumHeight);
                    _isMaxMediumHeightInitialized = true;
                }
                return _maxMediumHeight;
            }
        }



        private static bool _isMaxLargeLengthInitialized = false;
        private static int _maxLargeLength;
        /// <summary>
        /// Length limit of large size. Unit: mm.
        /// </summary>
        public static int MaxLargeLength
        {
            get
            {
                if (_isMaxLargeLengthInitialized == false)
                {
                    _maxLargeLength = int.Parse(Properties.Resources.MaxLargeLength);
                    _isMaxLargeLengthInitialized = true;
                }
                return _maxLargeLength;
            }
        }

        private static bool _isMaxLargeBreadthInitialized = false;
        private static int _maxLargeBreadth;
        /// <summary>
        /// Breadth limit of large size. Unit: mm.
        /// </summary>
        public static int MaxLargeBreadth
        {
            get
            {
                if (_isMaxLargeBreadthInitialized == false)
                {
                    _maxLargeBreadth = int.Parse(Properties.Resources.MaxLargeBreadth);
                    _isMaxLargeBreadthInitialized = true;
                }
                return _maxLargeBreadth;
            }
        }

        private static bool _isMaxLargeHeightInitialized = false;
        private static int _maxLargeHeight;
        /// <summary>
        /// Height limit of large size. Unit: mm.
        /// </summary>
        public static int MaxLargeHeight
        {
            get
            {
                if (_isMaxLargeHeightInitialized == false)
                {
                    _maxLargeHeight = int.Parse(Properties.Resources.MaxLargeHeight);
                    _isMaxLargeHeightInitialized = true;
                }
                return _maxLargeHeight;
            }
        }



        private static bool _isMaxLengthInitialized = false;
        private static int _maxLength;
        /// <summary>
        /// Length limit of current shipping policies. Unit: mm.
        /// </summary>
        public static int MaxLength
        {
            get
            {
                if (_isMaxLengthInitialized == false)
                {
                    _maxLength = int.Parse(Properties.Resources.MaxLength);
                    _isMaxLengthInitialized = true;
                }
                return _maxLength;
            }
        }

        private static bool _isMaxBreadthInitialized = false;
        private static int _maxBreadth;
        /// <summary>
        /// Breadth limit of current shipping policies. Unit: mm.
        /// </summary>
        public static int MaxBreadth
        {
            get
            {
                if (_isMaxBreadthInitialized == false)
                {
                    _maxBreadth = int.Parse(Properties.Resources.MaxBreadth);
                    _isMaxBreadthInitialized = true;
                }
                return _maxBreadth;
            }
        }

        private static bool _isMaxHeightInitialized = false;
        private static int _maxHeight;
        /// <summary>
        /// Height limit of current shipping policies. Unit: mm.
        /// </summary>
        public static int MaxHeight
        {
            get
            {
                if (_isMaxHeightInitialized == false)
                {
                    _maxHeight = int.Parse(Properties.Resources.MaxHeight);
                    _isMaxHeightInitialized = true;
                }
                return _maxHeight;
            }
        }

        private static bool _isMaxWeightInitialized = false;
        private static float _maxWeight;
        /// <summary>
        /// Weight limit of current shipping policies. Unit: kg.
        /// </summary>
        public static float MaxWeight
        {
            get
            {
                if (_isMaxWeightInitialized == false)
                {
                    _maxWeight = float.Parse(Properties.Resources.MaxWeight);
                    _isMaxWeightInitialized = true;
                }
                return _maxWeight;
            }
        }

        private static bool _isSmallPackageCostInitialized = false;
        private static decimal _smallPackageCost;
        /// <summary>
        /// Cost of a small package.
        /// </summary>
        public static decimal SmallPackageCost
        {
            get
            {
                if (_isSmallPackageCostInitialized == false)
                {
                    _smallPackageCost = decimal.Parse(Properties.Resources.SmallPackageCost);
                    _isSmallPackageCostInitialized = true;
                }
                return _smallPackageCost;
            }
        }

        private static bool _isMediumPackageCostInitialized = false;
        private static decimal _mediumPackageCost;
        /// <summary>
        /// Cost of a medium package.
        /// </summary>
        public static decimal MediumPackageCost
        {
            get
            {
                if (_isMediumPackageCostInitialized == false)
                {
                    _mediumPackageCost = decimal.Parse(Properties.Resources.MediumPackageCost);
                    _isMediumPackageCostInitialized = true;
                }
                return _mediumPackageCost;
            }
        }

        private static bool _isLargePackageCostInitialized = false;
        private static decimal _largePackageCost;
        /// <summary>
        /// Cost of a large package.
        /// </summary>
        public static decimal LargePackageCost
        {
            get
            {
                if (_isLargePackageCostInitialized == false)
                {
                    _largePackageCost = decimal.Parse(Properties.Resources.LargePackageCost);
                    _isLargePackageCostInitialized = true;
                }
                return _largePackageCost;
            }
        }

        #endregion



        /// <summary>
        /// Do some initialization. Because Singleton is utilized, the constructor is not accessible.
        /// </summary>
        /// <exception cref="ArgumentNullException">Occurs when a resource string is null.</exception>
        /// <exception cref="FormatException">Occurs when a resource string is not a correct int/float format.</exception>
        /// <exception cref="OverflowException">Occurs when a resource string represents a number exceeding
        /// the int/float limit.</exception>
        private PackageSolutionManager()
        {
            // Load package limits from resources.
            _maxSmallBreadth = int.Parse(Properties.Resources.MaxSmallBreadth);
            _maxSmallHeight = int.Parse(Properties.Resources.MaxSmallHeight);
            _maxSmallLength = int.Parse(Properties.Resources.MaxSmallLength);

            _maxMediumBreadth = int.Parse(Properties.Resources.MaxMediumBreadth);
            _maxMediumHeight = int.Parse(Properties.Resources.MaxMediumHeight);
            _maxMediumLength = int.Parse(Properties.Resources.MaxMediumLength);

            _maxLargeBreadth = int.Parse(Properties.Resources.MaxLargeBreadth);
            _maxLargeHeight = int.Parse(Properties.Resources.MaxLargeHeight);
            _maxLargeLength = int.Parse(Properties.Resources.MaxLargeLength);

            _maxLargeBreadth = int.Parse(Properties.Resources.MaxLargeBreadth);
            _maxLargeHeight = int.Parse(Properties.Resources.MaxLargeHeight);
            _maxLargeLength = int.Parse(Properties.Resources.MaxLargeLength);
        }

        private static PackageSolutionManager _manager;

        public static PackageSolutionManager Instance
        {
            get
            {
                if (_manager == null)
                {
                    try
                    {
                        _manager = new PackageSolutionManager();
                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException || ex is FormatException || ex is OverflowException)
                        {
                            Debug.WriteLine("Wrong number formats in Resources.resx file.");
                        }
                        else throw;
                    }
                }

                return _manager;
            }
        }

        /// <summary>
        /// Provide a solution for the given package based on its dimensions and weight.
        /// </summary>
        /// <param name="package"></param>
        /// <returns>Solution for the package.</returns>
        /// <exception cref="ArgumentNullException">Occurs when the input parameter is null.</exception>
        public PackageSolution ProvideSolution(Package package)
        {
            if (package == null) throw new ArgumentNullException("Parameter package could not be null.");

            var solution = new PackageSolution
            {
                PackageParsed = package,
                Cost = -1
            };

            // Step 1: process weight first.
            var isLegal = IsWeightLegal(solution);
            if (isLegal == false)
            {
                solution.PackageParsed.PackageType = PackageTypes.Unknown;
                return solution;
            }

            // Step 2: then process dimensions.
            DecidePackageType(solution);
            // if found the correct type, then calculate cost.
            if (solution.PackageParsed.PackageType != PackageTypes.Unknown)
            {
                CalculateSolutionCost(solution);
            }

            return solution;
        }

        /// <summary>
        /// Check whether the given weight is legal.
        /// </summary>
        /// <param name="weight">The given weight.</param>
        /// <param name="solution">Solution for the package. It is used to store an error message when necessary.</param>
        /// <returns>The weight is legal or not.</returns>
        /// <exception cref="ArgumentNullException">Occurs when the input parameter is null.</exception>
        private bool IsWeightLegal(PackageSolution solution)
        {
            if (solution == null) throw new ArgumentNullException("Parameter solution could not be null.");
            if (solution.PackageParsed == null)
                throw new ArgumentNullException("solution.PackageParsed could not be null.");

            if (solution.PackageParsed.Weight <= 0)
            {
                solution.ErrorMessage = "Weight should be larger than 0.";
                return false;
            }
            else if (solution.PackageParsed.Weight > MaxWeight)
            {
                solution.ErrorMessage = "Weight could not be over " + MaxWeight + " kg.";
                return false;
            }
            else return true;
        }

        /// <summary>
        /// Decide the type for the given package according to its dimensions.
        /// </summary>
        /// <param name="package">The given package.</param>
        /// <param name="solution">Solution for the package.</param>
        /// <exception cref="ArgumentNullException">Occurs when the input parameter is null.</exception>
        private void DecidePackageType(PackageSolution solution)
        {
            if (solution == null || solution.PackageParsed == null)
                throw new ArgumentNullException("Parameter package could not be null.");

            var package = solution.PackageParsed;
            var length = package.Length;
            var breadth = package.Breadth;
            var height = package.Height;

            // Case 1: 0 or negative integers.
            if (length <= 0 || breadth <= 0 || height <= 0)
            {
                package.PackageType = PackageTypes.Unknown;
                solution.ErrorMessage = "Dimensions should be larger than 0.";
                return;
            }

            // Case 2: length exceeds limit.
            if (length > MaxLength)
            {
                package.PackageType = PackageTypes.Unknown;
                solution.ErrorMessage = "Length could not exceed " + MaxLength + " mm.";
                return;
            }

            // Case 3: breadth exceeds limit.
            if (breadth > MaxBreadth)
            {
                package.PackageType = PackageTypes.Unknown;
                solution.ErrorMessage = "Breadth could not exceed " + MaxBreadth + " mm.";
                return;
            }

            // Case 4: height exceeds limit.
            if (height > MaxHeight)
            {
                package.PackageType = PackageTypes.Unknown;
                solution.ErrorMessage = "Height could not exceed " + MaxHeight + " mm.";
                return;
            }

            // Case 5: small package.
            if (length <= _maxSmallLength && breadth <= _maxSmallBreadth && height <= _maxSmallHeight)
            {
                package.PackageType = PackageTypes.Small;
            }

            // Case 6: medium package.
            if ((length > _maxSmallLength && length <= _maxMediumLength) ||
                (breadth > _maxSmallBreadth && breadth <= _maxMediumBreadth) ||
                (height > _maxSmallHeight && height <= _maxMediumHeight))
            {
                package.PackageType = PackageTypes.Medium;
            }

            // Case 7: Large package.
            if ((length > _maxMediumLength && length <= MaxLength) ||
                (breadth > _maxMediumBreadth && breadth <= MaxBreadth) ||
                (height > _maxMediumHeight && height <= MaxHeight))
            {
                package.PackageType = PackageTypes.Large;
            }
        }

        /// <summary>
        /// Calculate cost for the parsed package given in the input parameter solution and then store the cost in it.
        /// </summary>
        /// <param name="package">The given package.</param>
        /// <returns>Whether the calculation is successful.</returns>
        /// <exception cref="ArgumentNullException">Occurs when the input parameter is null.</exception>
        private bool CalculateSolutionCost(PackageSolution solution)
        {
            if (solution == null || solution.PackageParsed == null)
                throw new ArgumentNullException("Parameter package could not be null.");

            var succ = true;
            switch (solution.PackageParsed.PackageType)
            {
                case PackageTypes.Small:
                    solution.Cost = _smallPackageCost;
                    break;

                case PackageTypes.Medium:
                    solution.Cost = _mediumPackageCost;
                    break;

                case PackageTypes.Large:
                    solution.Cost = _largePackageCost;
                    break;

                case PackageTypes.Unknown:
                default:
                    solution.Cost = -1;
                    solution.ErrorMessage = "Could not calculate the cost of the package type PackageTypes.Unknown";
                    succ = false;
                    break;
            }

            return succ;
        }
    }
}
