﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParseTheParcel
{
    /// <summary>
    /// Package solution including information such package type and cost. Initial cost is -1.
    /// </summary>
    public class PackageSolution
    {
        public PackageSolution()
        {
            Cost = -1;
        }

        /// <summary>
        /// The package which is parsed by this solution.
        /// </summary>
        public Package PackageParsed { get; set; }
        /// <summary>
        /// Cost of this solution.
        /// </summary>
        public decimal Cost { get; set; }
        /// <summary>
        /// Can contains an error message if the solution does not exist and the package type is unknown.
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
