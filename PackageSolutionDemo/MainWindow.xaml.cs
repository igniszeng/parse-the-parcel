﻿using ParseTheParcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PackageSolutionDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void textBox_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            // if not numbers or dot
            if ((e.Key >= Key.D0 && e.Key <= Key.D9 ||
                e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9 ||
                e.Key == Key.OemPeriod || e.Key == Key.Decimal ||
                e.Key == Key.Left || e.Key == Key.Right ||
                e.Key == Key.Delete || e.Key == Key.Back) == false)
            {
                e.Handled = true;
            }
            else if (string.IsNullOrEmpty((sender as TextBox).Text))
            {
                // Don't allow 0 or dot as the first character
                if (e.Key == Key.D0 || e.Key == Key.NumPad0 || e.Key == Key.OemPeriod || e.Key == Key.Decimal)
                    e.Handled = true;
            }
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxLength.Text) || string.IsNullOrEmpty(textBoxBreadth.Text) ||
                string.IsNullOrEmpty(textBoxHeight.Text) || string.IsNullOrEmpty(textBoxWeight.Text))
                return;

            var length = 0;
            bool succ = int.TryParse(textBoxLength.Text, out length);
            var breadth = 0;
            succ &= int.TryParse(textBoxBreadth.Text, out breadth);
            var height = 0;
            succ &= int.TryParse(textBoxHeight.Text, out height);
            float weight = 0;
            succ &= float.TryParse(textBoxWeight.Text, out weight);
            if (succ == false)
                textBlockError.Text = "Please input numbers.";
            else
                textBlockError.Text = string.Empty;

            var manager = PackageSolutionManager.Instance;
            var package = new Package
            {
                Length = length,
                Breadth = breadth,
                Height = height,
                Weight = weight,
                PackageType = PackageTypes.Unknown
            };
            var solution = manager.ProvideSolution(package);
            if (solution != null)
            {
                if (solution.PackageParsed.PackageType == PackageTypes.Unknown)
                    textBlockError.Text = solution.ErrorMessage;
                else
                {
                    textBlockError.Text = string.Empty;
                    textBlockSolution.Text = solution.PackageParsed.PackageType.ToString();
                    textBlockCost.Text = solution.Cost.ToString();
                }
            }
        }
    }
}
