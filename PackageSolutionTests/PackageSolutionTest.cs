﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParseTheParcel;

namespace PackageSolutionTests
{
    [TestClass]
    public class PackageSolutionTest
    {
        [TestMethod]
        public void NewOfDefault()
        {
            var target = new PackageSolution();
            Assert.AreEqual(-1, target.Cost);
        }

        [TestMethod]
        public void NewAndMemberAccess()
        {
            var errorMessage = "error message";
            var package = new Package();
            var target = new PackageSolution
            {
                Cost = 1,
                ErrorMessage = errorMessage,
                PackageParsed = package
            };
            Assert.AreEqual(1, target.Cost);
            Assert.AreEqual(errorMessage, target.ErrorMessage);
            Assert.AreSame(package, target.PackageParsed);
        }
    }
}
