﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParseTheParcel;
using System.Collections.Generic;

namespace PackageSolutionTests
{
    [TestClass]
    public class PackageSolutionManagerTest
    {
        [TestMethod]
        public void StaticMembersInit()
        {
            Assert.AreNotEqual(0, PackageSolutionManager.MaxSmallBreadth);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxSmallHeight);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxSmallLength);

            Assert.AreNotEqual(0, PackageSolutionManager.MaxMediumBreadth);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxMediumHeight);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxMediumLength);

            Assert.AreNotEqual(0, PackageSolutionManager.MaxLargeBreadth);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxLargeHeight);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxLargeLength);

            Assert.AreNotEqual(0, PackageSolutionManager.MaxBreadth);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxHeight);
            Assert.AreNotEqual(0, PackageSolutionManager.MaxLength);

            Assert.AreNotEqual(0, PackageSolutionManager.MaxWeight);

            Assert.AreNotEqual(0, PackageSolutionManager.SmallPackageCost);
            Assert.AreNotEqual(0, PackageSolutionManager.MediumPackageCost);
            Assert.AreNotEqual(0, PackageSolutionManager.LargePackageCost);
        }

        [TestMethod]
        public void NewDefaultInstance()
        {
            var target = PackageSolutionManager.Instance;
            Assert.IsNotNull(target);
        }

        [TestMethod]
        public void SingletonTest()
        {
            var target1 = PackageSolutionManager.Instance;
            var target2 = PackageSolutionManager.Instance;
            Assert.AreSame(target1, target2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NullPackageForParsing()
        {
            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(null);
        }



        #region Tests of bad Weight

        [TestMethod]
        public void WeightNotLargerThanZero()
        {
            var package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = -1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Weight should be larger than 0.", solution.ErrorMessage);

            package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 0
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Weight should be larger than 0.", solution.ErrorMessage);
        }

        [TestMethod]
        public void WeightExceedsLimit()
        {
            var package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = PackageSolutionManager.MaxWeight + 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Weight could not be over " + PackageSolutionManager.MaxWeight + " kg.", solution.ErrorMessage);
        }

        #endregion



        #region Tests of bad Dimensions

        // Length
        [TestMethod]
        public void LengthNotLargerThanZero()
        {
            var package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = -1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Dimensions should be larger than 0.", solution.ErrorMessage);

            package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = 0,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Dimensions should be larger than 0.", solution.ErrorMessage);
        }

        [TestMethod]
        public void LengthExceedsLimit()
        {
            var package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = PackageSolutionManager.MaxLength + 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Length could not exceed " + PackageSolutionManager.MaxLength + " mm.", solution.ErrorMessage);
        }

        // Breadth
        [TestMethod]
        public void BreadthNotLargerThanZero()
        {
            var package = new Package
            {
                Breadth = -1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Dimensions should be larger than 0.", solution.ErrorMessage);

            package = new Package
            {
                Breadth = 0,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Dimensions should be larger than 0.", solution.ErrorMessage);
        }

        [TestMethod]
        public void BreadthExceedsLimit()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxBreadth + 1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Breadth could not exceed " + PackageSolutionManager.MaxBreadth + " mm.", solution.ErrorMessage);
        }

        // Breadth
        [TestMethod]
        public void HeightNotLargerThanZero()
        {
            var package = new Package
            {
                Breadth = 1,
                Height = -1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Dimensions should be larger than 0.", solution.ErrorMessage);

            package = new Package
            {
                Breadth = 1,
                Height = 0,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Dimensions should be larger than 0.", solution.ErrorMessage);
        }

        [TestMethod]
        public void HeightExceedsLimit()
        {
            var package = new Package
            {
                Breadth = 1,
                Height = PackageSolutionManager.MaxHeight + 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Unknown, solution.PackageParsed.PackageType);
            Assert.AreEqual("Height could not exceed " + PackageSolutionManager.MaxHeight + " mm.", solution.ErrorMessage);
        }

        #endregion



        #region Tests for package types and cost

        [TestMethod]
        public void SmallestSmallPackage()
        {
            var package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Small, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.SmallPackageCost, solution.Cost);
        }

        [TestMethod]
        public void LargestSmallPackage()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxSmallBreadth,
                Height = PackageSolutionManager.MaxSmallHeight,
                Length = PackageSolutionManager.MaxSmallLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Small, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.SmallPackageCost, solution.Cost);
        }



        [TestMethod]
        public void SmallestMediumPackages()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxSmallBreadth + 1,
                Height = PackageSolutionManager.MaxSmallHeight,
                Length = PackageSolutionManager.MaxSmallLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);

            package = new Package
            {
                Breadth = PackageSolutionManager.MaxSmallBreadth,
                Height = PackageSolutionManager.MaxSmallHeight + 1,
                Length = PackageSolutionManager.MaxSmallLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);

            package = new Package
            {
                Breadth = PackageSolutionManager.MaxSmallBreadth,
                Height = PackageSolutionManager.MaxSmallHeight,
                Length = PackageSolutionManager.MaxSmallLength + 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);
        }

        [TestMethod]
        public void LargestMediumPackage()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxMediumBreadth,
                Height = PackageSolutionManager.MaxMediumHeight,
                Length = PackageSolutionManager.MaxMediumLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);
        }

        [TestMethod]
        public void ShortestMediumSticks()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxSmallBreadth + 1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = PackageSolutionManager.MaxSmallHeight + 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = PackageSolutionManager.MaxSmallLength + 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);
        }

        [TestMethod]
        public void LongestMediumSticks()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxMediumBreadth,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = PackageSolutionManager.MaxMediumHeight,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = PackageSolutionManager.MaxMediumLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Medium, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.MediumPackageCost, solution.Cost);
        }



        [TestMethod]
        public void SmallestLargePackages()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxMediumBreadth + 1,
                Height = PackageSolutionManager.MaxMediumHeight,
                Length = PackageSolutionManager.MaxMediumLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);

            package = new Package
            {
                Breadth = PackageSolutionManager.MaxMediumBreadth,
                Height = PackageSolutionManager.MaxMediumHeight + 1,
                Length = PackageSolutionManager.MaxMediumLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);

            package = new Package
            {
                Breadth = PackageSolutionManager.MaxMediumBreadth,
                Height = PackageSolutionManager.MaxMediumHeight,
                Length = PackageSolutionManager.MaxMediumLength + 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);
        }

        [TestMethod]
        public void LargestLargePackage()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxLargeBreadth,
                Height = PackageSolutionManager.MaxLargeHeight,
                Length = PackageSolutionManager.MaxLargeLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);
        }

        [TestMethod]
        public void ShortestLargeSticks()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxMediumBreadth + 1,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = PackageSolutionManager.MaxMediumHeight + 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = PackageSolutionManager.MaxMediumLength + 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);
        }

        [TestMethod]
        public void LongestLargeSticks()
        {
            var package = new Package
            {
                Breadth = PackageSolutionManager.MaxLargeBreadth,
                Height = 1,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            var target = PackageSolutionManager.Instance;
            var solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = PackageSolutionManager.MaxLargeHeight,
                Length = 1,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);

            package = new Package
            {
                Breadth = 1,
                Height = 1,
                Length = PackageSolutionManager.MaxLargeLength,
                PackageType = PackageTypes.Unknown,
                Weight = 1
            };

            solution = target.ProvideSolution(package);
            Assert.AreEqual(PackageTypes.Large, solution.PackageParsed.PackageType);
            Assert.AreEqual(PackageSolutionManager.LargePackageCost, solution.Cost);
        }



        #endregion
    }
}
