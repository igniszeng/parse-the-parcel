﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParseTheParcel;

namespace PackageSolutionTests
{
    [TestClass]
    public class PackageTest
    {
        [TestMethod]
        public void NewOfDefaultConstructor()
        {
            var target = new Package();
            Assert.AreEqual(PackageTypes.Unknown, target.PackageType);
        }

        [TestMethod]
        public void NewAndMemberAccess()
        {
            var target = new Package
            {
                Length = 1,
                Breadth = 2,
                Height = 3,
                Weight = 4,
                PackageType = PackageTypes.Small
            };
            Assert.AreEqual(1, target.Length);
            Assert.AreEqual(2, target.Breadth);
            Assert.AreEqual(3, target.Height);
            Assert.AreEqual(4, target.Weight);
        }
    }
}
