﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParseTheParcel
{
    /// <summary>
    /// Three package types decided by the package dimensions.
    /// </summary>
    public enum PackageTypes
    {
        Small,
        Medium,
        Large,
        // If some error occurs, or the shipping policies are not clear, the type is unknown.
        Unknown
    }

    /// <summary>
    /// A package that is needed to be parsed.
    /// </summary>
    public class Package
    {
        public Package()
        {
            PackageType = PackageTypes.Unknown;
        }

        /// <summary>
        /// Length of the package. Unit: mm.
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// Breadth of the package. Unit: mm.
        /// </summary>
        public int Breadth { get; set; }
        /// <summary>
        /// Height of the package. Unit: mm.
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Weight of the package. Unit: kg.
        /// </summary>
        public float Weight { get; set; }
        /// <summary>
        /// Type of the package according to shipping policies.
        /// </summary>
        public PackageTypes PackageType { get; set; }
    }
}
