This solution contains 3 projects.

PackageSolutionManager is a class library which solves the required task.
PackageSolutionTests is the test project for PackageSolutionManager.
PackageSolutionDemo is a simple WPF project demonstating how to use the library PackageSolutionManager.